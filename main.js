var Hoover = /** @class */ (function () {
    function Hoover(x, y, orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }
    Hoover.prototype.move = function (instructions) {
        for (var _i = 0, instructions_1 = instructions; _i < instructions_1.length; _i++) {
            var instruction = instructions_1[_i];
            switch (instruction) {
                case 'D':
                    this.rotateRight();
                    break;
                case 'G':
                    this.rotateLeft();
                    break;
                case 'A':
                    this.advance();
                    break;
                default:
                    // Instruction invalide, peut être géré selon les besoins
                    break;
            }
        }
    };
    Hoover.prototype.rotateRight = function () {
        switch (this.orientation) {
            case 'N':
                this.orientation = 'E';
                break;
            case 'E':
                this.orientation = 'S';
                break;
            case 'S':
                this.orientation = 'W';
                break;
            case 'W':
                this.orientation = 'N';
                break;
        }
    };
    Hoover.prototype.rotateLeft = function () {
        switch (this.orientation) {
            case 'N':
                this.orientation = 'W';
                break;
            case 'W':
                this.orientation = 'S';
                break;
            case 'S':
                this.orientation = 'E';
                break;
            case 'E':
                this.orientation = 'N';
                break;
        }
    };
    Hoover.prototype.advance = function () {
        switch (this.orientation) {
            case 'N':
                this.y++;
                break;
            case 'E':
                this.x++;
                break;
            case 'S':
                this.y--;
                break;
            case 'W':
                this.x--;
                break;
        }
    };
    return Hoover;
}());
function getElementById(id) {
    var element = document.getElementById(id);
    if (!element) {
        alert("L'\u00E9l\u00E9ment avec l'ID '".concat(id, "' est introuvable."));
        throw new Error("L'\u00E9l\u00E9ment avec l'ID '".concat(id, "' est introuvable."));
    }
    return element;
}
var runButton = getElementById('run-button');
runButton.addEventListener('click', function () {
    var gridSizeX = parseInt(getElementById('grid-size-x').value, 10);
    var gridSizeY = parseInt(getElementById('grid-size-y').value, 10);
    var initialX = parseInt(getElementById('initial-x').value, 10);
    var initialY = parseInt(getElementById('initial-y').value, 10);
    var initialOrientation = getElementById('orientation').value;
    var instructions = getElementById('instructions').value;
    var hoover = new Hoover(initialX, initialY, initialOrientation);
    hoover.move(instructions);
    var resultElement = getElementById('result');
    resultElement.textContent = "Position finale : x=".concat(hoover.x, " y=").concat(hoover.y, " orientation=").concat(hoover.orientation);
});
