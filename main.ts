class Hoover {
  public x: number;
  public y: number;
  public orientation: string;

  constructor(x: number, y: number, orientation: string) {
    this.x = x;
    this.y = y;
    this.orientation = orientation;
  }

  move(instructions: string): void {
    for (const instruction of instructions) {
      switch (instruction) {
        case 'D':
          this.rotateRight();
          break;
        case 'G':
          this.rotateLeft();
          break;
        case 'A':
          this.advance();
          break;
        default:
          // Instruction invalide, peut être géré selon les besoins
          break;
      }
    }
  }

  private rotateRight(): void {
    switch (this.orientation) {
      case 'N':
        this.orientation = 'E';
        break;
      case 'E':
        this.orientation = 'S';
        break;
      case 'S':
        this.orientation = 'W';
        break;
      case 'W':
        this.orientation = 'N';
        break;
    }
  }

  private rotateLeft(): void {
    switch (this.orientation) {
      case 'N':
        this.orientation = 'W';
        break;
      case 'W':
        this.orientation = 'S';
        break;
      case 'S':
        this.orientation = 'E';
        break;
      case 'E':
        this.orientation = 'N';
        break;
    }
  }

  private advance(): void {
    switch (this.orientation) {
      case 'N':
        this.y++;
        break;
      case 'E':
        this.x++;
        break;
      case 'S':
        this.y--;
        break;
      case 'W':
        this.x--;
        break;
    }
  }
}

function getElementById<T extends HTMLElement>(id: string): T {
  const element = document.getElementById(id);
  if (!element) {
    alert(`L'élément avec l'ID '${id}' est introuvable.`);
    throw new Error(`L'élément avec l'ID '${id}' est introuvable.`);
  }
  return element as T;
}

const runButton = getElementById<HTMLButtonElement>('run-button');
runButton.addEventListener('click', function () {
  const gridSizeX = parseInt(getElementById<HTMLInputElement>('grid-size-x').value, 10);
  const gridSizeY = parseInt(getElementById<HTMLInputElement>('grid-size-y').value, 10);
  let initialX = parseInt(getElementById<HTMLInputElement>('initial-x').value, 10);
  let initialY = parseInt(getElementById<HTMLInputElement>('initial-y').value, 10);
  let initialOrientation = getElementById<HTMLSelectElement>('orientation').value;
  let instructions = getElementById<HTMLInputElement>('instructions').value;

  const hoover = new Hoover(initialX, initialY, initialOrientation);
  hoover.move(instructions);

  const resultElement = getElementById<HTMLInputElement>('result');
  resultElement.textContent = `Position finale : x=${hoover.x} y=${hoover.y} orientation=${hoover.orientation}`;
});
